using System;
using NBomber.CSharp;
using NBomber.Plugins.Http.CSharp;

namespace NBomberApp.Tests
{
    public class LockTest
    {
        public static void Run()
        {
            //Define any logic here
            //Create unique random hashes for each http request
            var stepLock = Step.Create("create_lock_step",
                clientFactory: HttpClientFactory.Create(),
                execute: context =>
                {
                    var request = 
                        Http.CreateRequest("GET", $"https://tvnetchk.ott.cytavision.com.cy/api/apps/lock?hash={Guid.NewGuid()}&mode=1")
                            .WithHeader("Accept", "text/html");

                    return Http.Send(request, context);
                });
            
            //scenario is basically a container for steps
            var scenario = ScenarioBuilder
                .CreateScenario("redis_test", stepLock)
                .WithoutWarmUp()
                .WithLoadSimulations(
                    Simulation.KeepConstant(copies: 5, during: TimeSpan.FromSeconds(160))
                );

            NBomberRunner
                .RegisterScenarios(scenario)
                .Run();
        }
    }
}