using System;
using Microsoft.Extensions.Configuration;

namespace IpConcurrency.Api.Configurations
{
    public static class ConfigurationBuilderExtensions
    {
        public static IConfigurationBuilder AddCommonConfiguration(this IConfigurationBuilder builder)
        {
            /*
             * We have to do it like this instead of hostingContext.HostingEnvironment.EnvironmentName
             * because `dotnet watch` overwrites the value 
             */
        
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";
            var configRootPath = Environment.GetEnvironmentVariable("CONFIG_ROOT") ?? "/Configs";
            var extraConfig = Environment.GetEnvironmentVariable("EXTRA_CONFIG_NAME");

            builder.AddJsonFile($"{configRootPath}/appsettings.json", false, false);
            builder.AddJsonFile($"{configRootPath}/appsettings.{env}.json");

            if (extraConfig != null) 
            {
                builder.AddJsonFile($"{configRootPath}/appsettings.{extraConfig}.json");
            }
            builder.AddEnvironmentVariables();
        
            return builder;
        }
    }
}