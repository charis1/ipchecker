﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using IpConcurrency.Api.ConfigMappers;
using IpConcurrency.Api.Constants;
using IpConcurrency.Api.Dto;
using IpConcurrency.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace IpConcurrency.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppsController : ControllerBase
    {
        private readonly ILogger<AppsController> _logger;
        private readonly IIpService _ipService;
        private readonly IRedisConcurrencyService _redisConcurrencyService;
        private readonly IOptions<SettingsConfiguration> _settingsConfiguration;
        
        public AppsController(ILogger<AppsController> logger, IIpService ipService, IRedisConcurrencyService redisConcurrencyService, IOptions<SettingsConfiguration> settingsConfiguration)
        {
            _logger = logger;
            _ipService = ipService;
            _redisConcurrencyService = redisConcurrencyService;
            _settingsConfiguration = settingsConfiguration;
        }
        
        /// <summary>
        /// "hash" is going to be used as the "redis key".
        /// A record is going to be inserted by "hash" as key and request's ip as value.
        /// </summary>
        /// <returns></returns>
        [HttpGet("lock")]
        public async Task<IActionResult> Lock([Required(AllowEmptyStrings = false)] string hash)
        {
            //Fetch stored ip by hash. If hash not found, null value is expected
            var subscriberIp = await _redisConcurrencyService.FetchByKeyAsync(hash);
            
            //Extract request's public IP 
            var requestIp = _ipService.GetRequestIp();
            
            if(subscriberIp == null || subscriberIp == requestIp)
            {
                //Add or Update(overwrite). We need to overwrite the existing value in order to reset the expire time of the record  
                await _redisConcurrencyService.InsertOrOverwriteAsync(hash, requestIp, _settingsConfiguration.Value.Expiration);

                
                _logger.LogInformation($"hash : {hash} inserted");
                return Ok(new ApiResult<IntervalRspDto>(new IntervalRspDto(_settingsConfiguration.Value.Interval)));
            }
            
            return Unauthorized(new ApiResult(ErrorCode.Unauthorized, "hash exists for different Ip"));
        }
        
        /// <summary>
        /// "hash" is going to be used as the "redis key".
        /// A record is going to be deleted by "hash".
        /// </summary>
        /// <returns></returns>
        [HttpGet("unlock")]
        public async Task<IActionResult> Unlock([Required(AllowEmptyStrings = false)] string hash)
        {
            await _redisConcurrencyService.RemoveByKeyAsync(hash);
            
            _logger.LogInformation($"hash : {hash} deleted");
            return Ok(new ApiResult("Record deleted successfully"));
        }
    }
}