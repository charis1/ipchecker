namespace IpConcurrency.Api.Dto
{
    public class IntervalRspDto
    {
        public int Interval { get; set; }
        
        public IntervalRspDto(int interval)
        {
            Interval = interval;
        }
    }
}