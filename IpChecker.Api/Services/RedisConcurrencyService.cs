using System;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace IpConcurrency.Api.Services
{
    public interface IRedisConcurrencyService
    { 
        Task<string> FetchByKeyAsync(string key);
        Task<bool> InsertOrOverwriteAsync(string key, string value, int expireInSecs);
        Task<bool> RemoveByKeyAsync(string key);
    }
    
    public class RedisConcurrencyService : IRedisConcurrencyService
    {
        private readonly IDatabase _redisDb;

        public RedisConcurrencyService(IConnectionMultiplexer redis)
        {
            _redisDb = redis.GetDatabase();
        }
        
        public async Task<string> FetchByKeyAsync(string key)
        {
            return await _redisDb.StringGetAsync(key);
        }

        public async Task<bool> InsertOrOverwriteAsync(string key, string value, int expireInSecs)
        {
            return await _redisDb.StringSetAsync(key: key, value: value, expiry:TimeSpan.FromSeconds(expireInSecs));
        }
        
        public async Task<bool> RemoveByKeyAsync(string key)
        {
            return await _redisDb.KeyDeleteAsync(key);
        }
    }
}