using Microsoft.AspNetCore.Http;

namespace IpConcurrency.Api.Services
{
    public interface IIpService
    {
        public string GetRequestIp();
    } 
    
    public class IpService : IIpService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IpService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetRequestIp()
        {
            return _httpContextAccessor.HttpContext.Request.Headers["X-Real-IP"];
        }
    }
}