using System;
using System.IO;
using System.Reflection;
using IpConcurrency.Api.ConfigMappers;
using IpConcurrency.Api.Filters;
using IpConcurrency.Api.Helpers;
using IpConcurrency.Api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace IpConcurrency.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IConnectionMultiplexer>(ConnectionMultiplexer.Connect(Configuration.GetConnectionString("RedisConnection")));
            services.Configure<SettingsConfiguration>(Configuration.GetSection("Settings"));
            
            services.AddControllers(options => {options.Filters.Add<ExceptionFilter>();})
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = BaseFirstContractResolver.Instance;
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
                
            services.AddTransient<IIpService, IpService>();
            services.AddTransient<IRedisConcurrencyService, RedisConcurrencyService>();
            services.AddHttpContextAccessor();
            //services.AddTransient<IMemoryCacheConcurrencyService, MemoryCacheConcurrencyConcurrencyService>();
            
            // Register the Swagger Generator service. This service is responsible for generating Swagger Documents.
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("api", new OpenApiInfo { Title = "Ip Concurrency API", Version = "v1"});
                
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("api/swagger.json", "API");

                c.RoutePrefix = "swagger";
            });
            
            //app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}