FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS base

WORKDIR /app

COPY IpChecker.Api/IpChecker.Api.csproj .

RUN dotnet restore

#Build final release
FROM base AS builder

COPY IpChecker.Api/* /app/

RUN dotnet publish -c Release -o /out

#Final
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS final

WORKDIR /out

COPY --from=builder /out .
COPY /Configs /Configs

CMD dotnet IpChecker.Api.dll
