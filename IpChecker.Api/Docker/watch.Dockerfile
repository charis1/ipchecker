FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS base

WORKDIR /app

COPY IpChecker.Api/IpChecker.Api.csproj .

RUN dotnet restore IpChecker.Api.csproj

#Copy everything else and build
COPY Configs/ ./Configs/
COPY IpChecker.Api/ ./IpChecker.Api/

#Run for development with watch
FROM base AS development

ENTRYPOINT dotnet watch -p ./IpChecker.Api/IpChecker.Api.csproj run