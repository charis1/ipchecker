namespace IpConcurrency.Api.Constants
{
    public enum ErrorCode
    {
        ServerError,
        Unauthorized
    }
}