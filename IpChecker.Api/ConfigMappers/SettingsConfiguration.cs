namespace IpConcurrency.Api.ConfigMappers
{
    public class SettingsConfiguration
    {
        public int Interval { get; set; }
        public int Expiration { get; set; }
    }
}